-- Enhanced by jh0nny 02/2019
-- AhFack Rotations, Rogue Subtlety -- PvE
-- Functions: Auto-Queue Accepter, Auto-Stealth, Auto-Poison. 
-- Modifiers: 
		-- CTRL = Kidney Shot at 4 Points. I'd suggest using this in High M+ quite frequently.
-- Macros:
		-- Macro has to be called "Aggro": /cast [@focus] Tricks of the Trade


local dark_addon = dark_interface
local SB = dark_addon.rotation.spellbooks.rogue
local lftime = 0 -- Dungeon / LFG Queue Timers
local x = 0 -- Counting seconds in resting
local y = 0 -- Counter for opener
local z = 0 -- Time in Combat
local v = 0 -- Smart Exsanguinate


local function gcd()
end

local function combat()

-------------------
-- Fetch Settings--
-------------------
local cds = dark_addon.settings.fetch('assass_cds', true)
local vanish = dark_addon.settings.fetch('assass_vanish.check', true)
local vanishpercent = dark_addon.settings.fetch('assass_vanish.spin', 10)
local shadow = dark_addon.settings.fetch('assass_shadow.check', true)
local shadowpercent = dark_addon.settings.fetch('assass_shadow.spin', 25)
local intpercent = math.random(65,80) --dark_addon.settings.fetch('assass_interrupt', 60)
local tott = dark_addon.settings.fetch('assass_tott', false)
local vial = dark_addon.settings.fetch('assass_vial.check', true)
local vialpercent = dark_addon.settings.fetch('assass_vial.spin', 45)
local feint = dark_addon.settings.fetch('assass_feint.check', true)
local feintpercent = dark_addon.settings.fetch('assass_feint.spin', 50)
local healthstone_percent = dark_addon.settings.fetch('assass_healthstone.spin', 35)
local use_healthstone = dark_addon.settings.fetch('assass_healthstone.check', true)
local use_healing_potion = dark_addon.settings.fetch('assass_healingpotion.check', true)


-----------------
-- Experimental--
-----------------
local AoE = 4
local inRange = enemies.around(5)


---------------
-- Defensives--
---------------
if vanish == true and castable(SB.Vanish) and player.health.percent < vanishpercent then
	return cast(SB.Vanish)
end
if shadow == true and castable(SB.CloakOfShadows) and player.health.percent < shadowpercent then 
	return cast(SB.CloakOfShadows)
end
if vial == true and castable(SB.CrimsonVial) and player.health.percent < vialpercent then 
	return cast(SB.CrimsonVial)
end
if feint == true and castable(SB.Feint) and player.health.percent < feintpercent then 
	return cast(SB.Feint)
end

-- Use Healthstone or BFA Healing Potion
if use_healthstone == true and GetItemCount(5512) >= 1 and GetItemCooldown(5512) == 0 and player.health.percent < healthstone_percent then
	macro('/use Healthstone')
end
if use_healing_potion == true and GetItemCount(152494) >= 1 and GetItemCooldown(152494) == 0 and GetItemCooldown(5512) > 0 and player.health.percent < healthstone_percent then
	macro('/use Coastal Healing Potion')
end

-- Start Auto Attack
if target.alive and target.enemy and player.alive and not player.channeling() and target.distance < 8 then
    auto_attack()

---------------
-- Interrupts--
---------------
if toggle('interrupts', false) and target.castable(SB.Kick, 'target') == 0 and target.interrupt(intpercent, false) then
	return cast(SB.Kick, 'target')
end


--------------
-- Modifiers--
--------------
if modifier.control and target.castable(SB.KidneyShot) and player.power.combopoints.actual >= 4 then
	return cast(SB.KidneyShot)
end


------------
-- Utility--
------------
-- Tricks of the Trade
if tott == true and -spell(SB.TricksOfTheTrade) == 0 then
	return RunMacro("Aggro")
end

-- Use Trinkets
local start, duration, enable = GetInventoryItemCooldown("player", 13)
if enable == 1 and start == 0 then
	return macro('/use 13')
end   
local start, duration, enable = GetInventoryItemCooldown("player", 14)
if enable == 1 and start == 0 then
	return macro('/use 14')
end



-------------------------
-- Smart Spread Garrote--
-------------------------
if toggle('multitarget', false) and player.buff(SB.Subterfuge).up then
	local enemies_around = enemies.around(5)

	local enemy_for_garrote = enemies.match(function (unit)
		return unit.alive and unit.combat and unit.distance <= 5 and unit.debuff(SB.Garrote).down
	end)

	if target.debuff(SB.Garrote).up and enemy_for_garrote and enemy_for_garrote.name then
		for i=1,enemies_around do
			macro('/target ' .. enemy_for_garrote.name)
			if target.guid == enemy_for_garrote.guid and target.debuff(SB.Garrote).down then break end
		end
	end

	if target.debuff(SB.Garrote).down or target.debuff(SB.Garrote).remains <= 5.4 then
		return cast(SB.Garrote)
	end
end


-----------------------
-- Smart Exsanguinate--
-----------------------
if talent (6,3) and not v == 99 and target.castable(SB.Exsanguinate) then
	if target.castable(SB.Exsanguinate) and target.castable(SB.Rupture) and player.power.combopoints.actual >= 4 and target.debuff(SB.Rupture).remains < 6 and v == 0 then
		v = v + 1
		print ("EXSANGUINATE START")
		return cast(SB.Rupture)
	end
	if target.castable(SB.Exsanguinate) and target.castable(SB.Garrote) and target.debuff(SB.Garrote).remains < 5.4 and v == 1 then
		return cast(SB.Garrote)
	end
	if target.castable(SB.Exsanguinate) and target.debuff(SB.Garrote).remains > 15 and target.debuff(SB.Rupture).remains > 25 and v == 1 then
		v = 99
		print ("EXSANGUINATE DONE")
		return cast(Exsanguinate)
	end
end


-----------------------
-- Exsanguinate Build--
-----------------------
if talent(6,3) then
	if not target.debuff(SB.Rupture).exists and player.power.combopoints.actual >= 4 and player.power.energy.actual >= 25 and not lastcast(SB.Exsanguinate) then
		v = 0
		return cast(SB.Rupture, 'target')
	end
	if not target.debuff(SB.Garrote).exists and -spell(SB.Garrote) == 0 and player.power.combopoints.actual <= 4 and player.power.energy.actual >= 45 and not lastcast(SB.Exsanguinate) then
		v = 0
		return cast(SB.Garrote)
	end
	if target.debuff(SB.Rupture).remains < 2 and player.power.combopoints.actual >= 4 and player.power.energy.actual >= 25 and v == 0 and -spell(SB.Rupture) == 0 then
		return cast(SB.Rupture)
	end
	if target.debuff(SB.Garrote).remains < 5.4 and player.power.energy.actual >= 45 and v == 0 and -spell(SB.Garrote) == 0 then
		return cast(SB.Garrote)
	end
	if not -buff(SB.Subterfuge) and -spell(SB.Vanish) == 0 and player.power.energy.actual >= 45 and -spell(SB.Garrote) == 0 and target.debuff(SB.Garrote).remains < 5.4 and -spell(SB.Exsanguinate) == 0 then 
		return cast(SB.Vanish)
	end
	if -buff(SB.Subterfuge) and player.power.energy.actual >= 45 and -spell(SB.Garrote) == 0 and target.debuff(SB.Garrote).remains < 5.4 then
		return cast(SB.Garrote)
	end
	if talent(7,3) and player.power.energy.actual >= 35 and not -target.debuff(SB.CrimsonTempest) and player.power.combopoints >= 4 and -spell(SB.CrimsonTempest) == 0 then
		return cast(SB.CrimsonTempest)
	end
	if -spell(SB.Envenom) == 0 and player.power.combopoints.actual >= 4 and player.power.energy.actual >= 35 then
		return cast(SB.Envenom) 
	end
	if -spell(SB.FanOfKnives) == 0 and player.power.energy.actual >= 35 and inRange >= 4 then
		return cast(SB.FanOfKnives)
	end
	if -spell(SB.Mutilate) == 0 and player.power.energy.actual >= 50 and inRange <= 3 then
		return cast(SB.Mutilate)
	end	
end


----------------------
-- Toxic Blade Build--
----------------------
if talent(6,2) then
	if target.debuff(SB.Garrote).down and player.power.combopoints.actual <= 4 and target.castable(SB.Garrote) then
		return cast(SB.Garrote)
	end
	if target.debuff(SB.Rupture).remains <= 2 and player.power.combopoints.actual >= 4 and target.castable(SB.Rupture) then
		return cast(SB.Rupture)
	end
	if player.buff(SB.Subterfuge).down and castable(SB.Vanish) and target.castable(SB.Garrote) and target.debuff(SB.Garrote).remains <= 5.4 then 
		return cast(SB.Vanish)
	end
	
	if toggle('cooldowns', false) and target.castable(SB.Vendetta) and target.debuff(SB.Garrote).up and target.debuff(SB.Rupture).up then
		return cast(SB.Vendetta, 'target')
	end
	
	if target.castable(SB.ToxicBlade) then
		return cast(SB.ToxicBlade)
	end
	if talent(7,3) and target.debuff(SB.CrimsonTempest).down and player.power.combopoints >= 4 and castable(SB.CrimsonTempest) then
		return cast(SB.CrimsonTempest)
	end
	if target.castable(SB.Envenom) and player.power.combopoints.actual >= 4 then
		return cast(SB.Envenom) 
	end
	if target.castable(SB.FanOfKnives) and inRange >= AoE then
		return cast(SB.FanOfKnives)
	end
	if target.castable(SB.Mutilate) and inRange < AoE then
		return cast(SB.Mutilate)
	end
end

end

end


local function resting()

if lastcast(SB.Vanish) and target.castable(SB.Garrote) then
	return cast(SB.Garrote, 'target')
end

-----------------
-- Auto Utility--
-----------------
if player.buff(SB.Stealth).down and castable(SB.Stealth) and player.alive then
	y = 0
	return cast(SB.Stealth)
end
if player.buff(SB.DeadlyPoison).down and castable(SB.DeadlyPoison) and player.alive then
    return cast(SB.DeadlyPoison)
end
if player.buff(SB.CripplingPoison).down and castable(SB.CripplingPoison) and player.alive then
    return cast(SB.CripplingPoison)
end

--------------
-- Auto Join--
--------------
local lfg = GetLFGProposal();
local hasData = GetLFGQueueStats(LE_LFG_CATEGORY_LFD);
local hasData2 = GetLFGQueueStats(LE_LFG_CATEGORY_LFR);
local hasData3 = GetLFGQueueStats(LE_LFG_CATEGORY_RF);
local hasData4 = GetLFGQueueStats(LE_LFG_CATEGORY_SCENARIO);
local hasData5 = GetLFGQueueStats(LE_LFG_CATEGORY_FLEXRAID);
local hasData6 = GetLFGQueueStats(LE_LFG_CATEGORY_WORLDPVP);
local bgstatus = GetBattlefieldStatus(1);
local autoaccept = dark_addon.settings.fetch('assass_autoaccept.check')


if hasData == true or hasData2 == true or hasData4 == true or hasData5 == true or hasData6 == true or bgstatus == "queued" then
	SetCVar ("Sound_EnableSoundWhenGameIsInBG",1)
elseif hasdata == nil or hasData2 == nil or hasData3 == nil or hasData4 == nil or hasData5 == nil or hasData6 == nil or bgstatus == "none" then
	SetCVar ("Sound_EnableSoundWhenGameIsInBG",0)
end

if lfg == true or bgstatus == "confirm" and autoaccept == true then
	PlaySound(SOUNDKIT.IG_PLAYER_INVITE, "Dialog", false);
	lftime = lftime + 1
end

if lftime >=math.random(20,35) and autoaccept == true then
	SetCVar ("Sound_EnableSoundWhenGameIsInBG",0)
	macro('/click LFGDungeonReadyDialogEnterDungeonButton')
	lftime = 0
end
end


local function interface()
	local settings = {
		key = 'assass',
		title = 'AhFack jh0nny Rogue Assassination',
		width = 250,
		height = 500,
		resize = true,
		show = false,
		template = {
			{ type = 'header', text = 'Assassination Settings' },
			{ type = 'rule' },   
			{ type = 'rule' },   
			{ type = 'text', text = 'Defensives' },
			{ key = 'vanish', type = 'checkspin', text = 'Vanish at HP%', desc = 'What % you will be using Vanish at', default_spin = 10, default_check = true, min = 10, max = 100, step = 5 },
			{ key = 'shadow', type = 'checkspin', text = 'Cloak of Shadows HP%', desc = 'What % you will be using Cloak of Shadows at', default_spin = 20, default_check = true, min = 10, max = 100, step = 5 },
			{ key = 'vial', type = 'checkspin', text = 'Vial of Crimson HP%', desc = 'What % you will be using Vial of Crimson at', default_spin = 45, default_check = true, min = 10, max = 100, step = 5 },
			{ key = 'feint', type = 'checkspin', text = 'Feint at HP%', desc = 'What % you will be using Feint at', default_spin = 50, default_check = true, min = 10, max = 100, step = 5 },
			{ key = 'healthstone', type = 'checkspin', text = 'Healthstone at HP%', desc = 'What % you will be using Healthstones at', default_spin = 35, default_check = true, min = 10, max = 100, step = 5 },
			{ key = 'healingpotion', type = 'checkbox', text = 'Use Healing Potion?', desc = 'Use Healing Potion', default_spin = 35, default = true },
			{ type = 'rule' },
			{ type = 'text', text = 'Utility' },
			{ key = 'tott', type = 'checkbox', text = 'Tricks of the Trade', desc = 'Use Macro to cast TotT on Focus/Tank' },
			{ key = 'autoaccept', type = 'checkbox', text = 'Auto Accept Queue', desc = 'Auto Accepts any Queue for BGs and DGs'},
		}
	} 
	
	configWindow = dark_addon.interface.builder.buildGUI(settings) 

	dark_addon.interface.buttons.add_toggle({
		name = 'opener',
		label = 'Opener',
		font = 'dark_addon_icon',
		on = {
			label = dark_addon.interface.icon('bars'),
			color = dark_addon.interface.color.green,
			color2 = dark_addon.interface.color.dark_green
		},
		off = {
			label = dark_addon.interface.icon('bars'),
			color = dark_addon.interface.color.grey,
			color2 = dark_addon.interface.color.dark_grey
		}
	})

	dark_addon.interface.buttons.add_toggle({
		name = 'settings',
		label = 'Rotation Settings',
		font = 'dark_addon_icon',
		on = {
			label = dark_addon.interface.icon('cog'),
			color = dark_addon.interface.color.grey,
			color2 = dark_addon.interface.color.dark_grey
		},
		off = {
			label = dark_addon.interface.icon('cog'),
			color = dark_addon.interface.color.blue,
			color2 = dark_addon.interface.color.ratio(dark_addon.interface.color.blue, 0.7)
		},
		callback = function(self)
			if configWindow.parent:IsShown() then
				configWindow.parent:Hide()
			else
				configWindow.parent:Show()
			end
		end
	})
end

dark_addon.rotation.register({
	spec = dark_addon.rotation.classes.rogue.assassination,
	name = 'AHassassination',
	label = 'AhFack Rotations',
	gcd = gcd,
	combat = combat,
	resting = resting,
	interface = interface
})